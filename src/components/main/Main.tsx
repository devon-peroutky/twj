import React from 'react';
import './main.scss';

function Main() {
    return (
        <div className="main-container">
            <div className="main-content">
                <h1>Trippin' With Jamie</h1>
                <div className={"bar"}/>
                <span>Turn your dreams into memories</span>
            </div>
        </div>
    );
}

export default Main;
